<?php
include 'config.php';
if($_SERVER["REQUEST_METHOD"] == "POST"){
  $deletePhaseCall=$connection->prepare("DELETE FROM Phase WHERE phaseName= ? AND projectID= ?");
  $deletePhaseCall->bind_param("si", $_GET['phaseName'], $_GET['id']);
  $deletePhaseCall->execute();

  if($deletePhaseCall->affected_rows===1){
    echo "Phase has been deleted successfully.";
    echo "<a href=" . '"phases.php"'. '"> Back</a>';
  }
  else{
    echo "Error";
    echo "<a href=" . '"phases.php"'. '"> Back</a>';
  }
}
$deletePhaseCall->close();

function seoUrlReverse($string){
$string = preg_replace("/[-]/", " ", $string);
$string = preg_replace("/[_]+/", "-", $string);
$string=ucwords($string);
return $string;
}

?>
