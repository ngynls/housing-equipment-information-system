<?php
	session_start();
// retrieve the login_user from session.php so that the user can access multiple pages
	if (isset($_SESSION['login_user'])) {
		$loginSuccess = true;
	}
	else {
		$loginSuccess = false;
	}
?>

<html>
<!--HEAD-->
<head>
  <meta charset = "utf-8" />
	<link rel="stylesheet" type="text/css" href="css/dashboard.css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
	<title>Damavand Housing and Condo Company Information System- [Dashboard]</title>
</head>
<!-- END HEAD-->
<!--- BODY ----->
<body>

  <!--Navbar -->
  <div class="navigationBar">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Damavand Information System</a>
      <!--Toggler-->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapsable" aria-controls="navbarCollapsable" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!--END Toggler-->
      <!--Collapsable nav -->
      <div class="collapse navbar-collapse" id="navbarCollapsable">
         <a class="nav-link" href="myDashboard.php"><i class="fas fa-tachometer-alt"></i>  Dashboard</a>
         <?php
           if ($_SESSION['WhetherAdmin'] == 'True')
           {
             echo "<a class=". '"nav-link"' . "href=" . '"projects.php"' . "><i class=" .'"fas fa-project-diagram"'."></i>  Add/Update a project</a>";
             echo "<a class=". '"nav-link"' . "href=" . '"phases.php"' . "><i class=".'"fas fa-wrench"'."></i>  Add/Update a phase</a>";
             echo "<a class=" . '"nav-link"' . "href=" . '"reportAdmin.php"' . "><i class=" . '"fas fa-file"' ."></i>  Reports</a>";
             echo "<a class=" . '"nav-link"' . "href=" . '"quotation.php"' . "><i class=" . '"fas fa-file-invoice-dollar"' ."></i>  Quotations</a>";
           }
           else
           {
             echo "<a class=" . '"nav-link"' ."href=" .'"reportCust.php"'. ">  My Project</a>";
           }
         ?>
         <div class="navbar-text ml-auto" >
           <span class="navbar-text">
             <?php
               echo 'Welcome ' . $_SESSION['login_user'] . '<br />';
             ?>
           </span>
         </div>
         <a class="nav-link" href="logout.php"><i class="fas fa-sign-out-alt"></i>  Logout</a>
      </div>
      <!--Collapsable nav -->
    </nav>

  </div>
<!--END NavBar -->

<!--Main -->
	<div class="main">

    <!--Content -->
    <ol class="breadcrumb">
      <li>Reports</li>
    </ol>

    <section id="main-content">
			<?php
						include 'config.php';

					// Fetch data into the dropdown list
					$user = $_SESSION['login_user'];
					$sql="select p.projectID, p.name " .
						"from Project as p, Contract as c ".
						"where c.userID='$user' and c.projectID=p.projectID";
					$resultID = mysqli_query($connection,$sql);

					echo '<form action="" method="POST">';
					echo "<select name='optionName'>";
					while ($row = mysqli_fetch_assoc($resultID))
					{
						unset($id, $name);

						$id = $row['projectID'];
						$name = $row['name'];
						echo '<option value="'.$id.'">'.$name.'</option>';
					}

					echo "</select>";
					echo "<input name='submit' type='submit' value='Submit' />";
					echo "</form>";


					// Validate the dropdown list selection (which project) and print the reports
					if($_SERVER['REQUEST_METHOD'] =='POST')
					{
						$optionName=$_POST['optionName'];

						// Contract report
						$qry = "Select p.name, c.contractNumber, cd.typeOfContract, cd.dateOfContract, cd.cost, u.FirstName, u.LastName"
						." from Users as u, Project as p, Contract as c, ContractDetailed as cd"
						." where p.projectID='$optionName' and c.projectID='$optionName' and c.contractNumber=cd.contractNumber and c.userID=u.UserID";
						$result = mysqli_query($connection,$qry);

						echo "<p style='color:blue; font-size: 20px;'  >Contract Report </p>";
						echo "<div class='table-responsive-sm'>";
						echo "<table class='table table-striped'>";
						echo "<tr>
							<th>Project Name</th>
							<th>Contract Number</th>
							<th>Contract Type</th>
							<th>Contract Date</th>
							<th>Cost</th>
							<th>Owner's First Name</th>
							<th>Owner's Last Name</th>
							</tr>";

						while($row=mysqli_fetch_assoc($result)){
							echo "<tr>";
							echo "<td>".$row['name']."</td>";
							echo "<td>".$row['contractNumber']."</td>";
							echo "<td>".$row['typeOfContract']."</td>";
							echo "<td>".$row['dateOfContract']."</td>";
							echo "<td>".$row['cost']."</td>";
							echo "<td>".$row['FirstName']."</td>";
							echo "<td>".$row['LastName']."</td>";
							echo "</tr>";
						}
						echo "</table>";
						echo "</div>";

						// Project Overall report
						echo "<p style='color:blue; font-size: 20px;'  >Project Description</p>";
						echo "<div class='table-responsive-sm'>";
						echo "<table class='table table-striped'>
							<tr>
							<th>Project ID</th>
							<th>Name</th>
							<th>Type</th>
							<th>Current Status</th>
							</tr>";

						$qryProj = "Select p.projectID, p.name, p.type, p.currentStatus"
						." from Project as p"
						." where p.projectID='$optionName'";

						$resultProj = mysqli_query($connection,$qryProj);

						while($rowProj=mysqli_fetch_assoc($resultProj)){
							echo "<tr>";
							echo "<td>".$rowProj['projectID']."</td>";
							echo "<td>".$rowProj['name']."</td>";
							echo "<td>".$rowProj['type']."</td>";
							echo "<td>".$rowProj['currentStatus']."</td>";
							echo "</tr>";
						}
						echo "</table>";
						echo "</div>";

							mysqli_close($connection);
					}
					?>
    </section>
    <!--END Content -->

	</div>
<!--END Main -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
<!--- END BODY ----->
</html>
