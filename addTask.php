<?php
 	include 'config.php';

	if($_SERVER["REQUEST_METHOD"] == "POST"){

    $addTaskCall=$connection->prepare("INSERT INTO Task (taskName, taskDate, projectID, phaseName, taskCost, time) VALUES(?,?,?,?,?,?)");
    $addTaskCall->bind_param("ssisdi",$_POST['taskName'],$_POST['taskDate'],$_POST['projectID'],$_POST['phaseName'],$_POST['taskCost'],$_POST['time']);
    $addTaskCall->execute();

		if($addTaskCall->affected_rows===1){
      echo "Task has been saved.";
      echo "<a href=" . '"tasks.php"'. '"> Back</a>';
    }
		else{
      echo "Error";
      echo "<a href=" . '"tasks.php"'. '"> Back</a>';
		}
	}
  $addTaskCall->close();
?>
