<?php
include 'config.php';
if($_SERVER["REQUEST_METHOD"] == "POST"){
  $deleteContractCall=$connection->prepare("DELETE FROM ContractDetailed WHERE contractNumber= ?");
  $deleteContractCall->bind_param("i", $_GET['contractNumber']);
  $deleteContractCall->execute();

  if($deleteContractCall->affected_rows===1){
    echo "Contract has been deleted successfully.";
    echo "<a href=" . '"contracts.php"'. '"> Back</a>';
  }
  else{
    echo "Error";
    echo "<a href=" . '"contracts.php"'. '"> Back</a>';
  }
}
$deleteContractCall->close();

?>
