<?php
 	include 'config.php';

	if($_SERVER["REQUEST_METHOD"] == "POST"){

    $addContractDetailedCall=$connection->prepare("INSERT INTO ContractDetailed (typeOfContract,dateOfContract,cost) VALUES(?,?,?)");
    $retrieveContractNumber=$connection->prepare("SELECT contractNumber FROM ContractDetailed ORDER BY dateOfContract DESC LIMIT 1");
    $addContractCall=$connection->prepare("INSERT INTO Contract (contractNumber, projectID, UserID) VALUES(?,?,?)");

    $addContractDetailedCall->bind_param("ssd", $_POST['typeOfContract'],$_POST['dateOfContract'],$_POST['cost']);
    $addContractDetailedCall->execute();

    if($addContractDetailedCall->affected_rows===1){
      echo "Contract details has been saved.\n";
      //echo "<a href=" . '"contracts.php"'. '"> Back</a>';
    }
    else{
      echo "Error, contract details couldn't be saved\n";
      //echo $addContractDetailedCall->error;
      //echo "<a href=" . '"contracts.php"'. '"> Back</a>';
    }

    $retrieveContractNumber->execute();
    $retrieveContractNumberResult=$retrieveContractNumber->get_result();
    $contractNumber=0;
    while($rowContractNumber=mysqli_fetch_assoc($retrieveContractNumberResult)){
      $contractNumber=$rowContractNumber['contractNumber'];
    }

    $addContractCall->bind_param("iis",$contractNumber,$_POST['projectID'],$_POST['userID']);
    $addContractCall->execute();

    if($addContractCall->affected_rows===1){
      echo "Contract has been saved.\n";
      echo "<a href=" . '"contracts.php"'. '"> Back</a>';
    }
    else{
      echo "Error, contract couldn't be saved\n";
      echo "<a href=" . '"contracts.php"'. '"> Back</a>';
    }

    $addContractCall->close();
    $retrieveContractNumber->close();
    $addContractDetailedCall->close();
	} //end of if statement checking for POST
?>
