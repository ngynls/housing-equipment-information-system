<?php
include 'config.php';
//TODO:Need to figure out how to handle phase name that has more than 1 word
if($_SERVER["REQUEST_METHOD"] == "POST"){

  $oldName=seoUrlReverse($_GET['phaseName']);
  $oldID=$_GET['id'];

  $updatePhaseCall=$connection->prepare("UPDATE Phase SET phaseName= ?, projectID= ?, estmCost= ? , actualCost= ? , estmTime= ?, actualTime= ?
  WHERE phaseName='$oldName' AND projectID='$oldID'");
  $updatePhaseCall->bind_param("siddii",$_POST['phaseName'],$_POST['projectID'],$_POST['estmCost'],$_POST['actualCost'],$_POST['estmTime'],$_POST['actualTime']);
  $updatePhaseCall->execute();

  if($updatePhaseCall->affected_rows===1){
    echo "Phase has been updated.";
    echo "<a href=" . '"phases.php"'. '"> Back</a>';
  }
  else{
    echo "Error";
    echo "<a href=" . '"phases.php"'. '"> Back</a>';
  }
}
$updatePhaseCall->close();

function seoUrlReverse($string){
$string = preg_replace("/[-]/", " ", $string);
$string = preg_replace("/[_]+/", "-", $string);
$string=ucwords($string);
return $string;
}
?>
