<?php
	session_start();
// retrieve the login_user from session.php so that the user can access multiple pages
	if (isset($_SESSION['login_user'])) {
		$loginSuccess = true;
	}
	else {
		$loginSuccess = false;
	}
?>
<html>
<!--HEAD-->
<head>
  <meta charset = "utf-8" />
	<link rel="stylesheet" type="text/css" href="css/dashboard.css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
	<title>Damavand Housing and Condo Company Information System- [Dashboard]</title>
</head>
<!-- END HEAD-->
<!--- BODY ----->
<body>

  <!--Navbar -->
  <div class="navigationBar">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Damavand Information System</a>
      <!--Toggler-->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapsable" aria-controls="navbarCollapsable" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!--END Toggler-->
      <!--Collapsable nav -->
      <div class="collapse navbar-collapse" id="navbarCollapsable">
         <a class="nav-link" href="myDashboard.php"><i class="fas fa-tachometer-alt"></i>  Dashboard</a>
         <?php
           if ($_SESSION['WhetherAdmin'] == 'True')
           {
             echo "<a class=". '"nav-link"' . "href=" . '"projects.php"' . "><i class=" .'"fas fa-project-diagram"'."></i>  Project</a>";
						 echo "<a class=". '"nav-link"' . "href=" . '"contracts.php"' . "><i class=".'"fas fa-scroll"'."></i>  Contract</a>";
             echo "<a class=". '"nav-link"' . "href=" . '"phases.php"' . "><i class=".'"fas fa-wrench"'."></i>  Phase</a>";
						 echo "<a class=". '"nav-link"' . "href=" . '"tasks.php"' . "><i class=".'"fas fa-wrench"'."></i>  Tasks</a>";
             echo "<a class=" . '"nav-link"' . "href=" . '"reportAdmin.php"' . "><i class=" . '"fas fa-file"' ."></i>  Reports</a>";
             echo "<a class=" . '"nav-link"' . "href=" . '"quotation.php"' . "><i class=" . '"fas fa-file-invoice-dollar"' ."></i>  Quotations</a>";
           }
           else
           {
             echo "<a class=" . '"nav-link"' ."href=" .'"reportCust.php"'. ">  My Project</a>";
           }
         ?>
         <div class="navbar-text ml-auto" >
           <span class="navbar-text">
             <?php
               echo 'Welcome ' . $_SESSION['login_user'] . '<br />';
             ?>
           </span>
         </div>
         <a class="nav-link" href="logout.php"><i class="fas fa-sign-out-alt"></i>  Logout</a>
      </div>
      <!--Collapsable nav -->
    </nav>

  </div>
<!--END NavBar -->

<!--Main -->
	<div class="main">

    <!--Content -->
    <ol class="breadcrumb">
      <li>Phases</li>
    </ol>

    <section id="main-content">
      <a href='#' data-toggle='modal' data-target='#addModal'> Add a new phase</a>
      <?php
        include 'config.php';

        if($_SESSION['WhetherAdmin'] == 'True'){
          include 'inc/displayPhasesTablesForAdmins.php';
        }
      ?>
    </section>

		<!--Add Phase Modal -->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add a new phase </h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form method="post" action="addPhase.php">
									<div class="form-group">
										<label for="input-phaseName">Phase name</label>
										<input type="text" class="form-control" name="phaseName" placeholder="Enter the phase name">
									</div>
                  <div class="form-group">
                    <label for="input-projectID">Project ID</label>
                    <input type="text" class="form-control" name="projectID" placeholder="Enter the project ID">
                  </div>
                  <div class="form-group">
                    <label for="input-estmCost">Estimate cost</label>
                    <input type="text" class="form-control" name="estmCost" placeholder="Enter the estimate cost (two decimals, no $)">
                  </div>
                  <div class="form-group">
                    <label for="input-actualCost">Actual cost</label>
                    <input type="text" class="form-control" name="actualCost" placeholder="Enter the actual cost (two decimals, no $)">
                  </div>
                  <div class="form-group">
                    <label for="input-estmTime">Estimate time</label>
                    <input type="text" class="form-control" name="estmTime" placeholder="Enter the estimate time (in h)">
                  </div>
                  <div class="form-group">
                    <label for="input-actualTime">Actual time</label>
                    <input type="text" class="form-control" name="actualTime" placeholder="Enter the actual time (in h)">
                  </div>
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save changes</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--End Add Phase Modal -->

    <!--END Content -->

	</div>
<!--END Main -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
<!--- END BODY ----->
</html>
