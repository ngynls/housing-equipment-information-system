<?php
include 'config.php';

$addQuotationCall=$connection->prepare("INSERT INTO Quotation (dateOfQuote, timeOfQuote, supplierName, itemID, projectID, phaseName, taskName, quantity, amountRemaining)
VALUES(?,?,?,?,?,?,?,?,?)");
$itemQuantityInDollarsCall=$connection->prepare("SELECT supplierName, price from Item WHERE itemID= ?");

$currentDay=date("Y-m-d");
$currentTime=date("h:i:s");

$itemQuantityInDollarsCall->bind_param("i",$_POST['itemID']);
$itemQuantityInDollarsCall->execute();
$findQuantityInDollarsResult=$itemQuantityInDollarsCall->get_result();
$priceForOneItem=0;
$supplierName='';

while($rowPrice=mysqli_fetch_assoc($findQuantityInDollarsResult)){
  $priceForOneItem=$rowPrice['price'];
  $supplierName=$rowPrice['supplierName'];
}
$amountRemaining=$priceForOneItem*$_POST['quantity'];

$addQuotationCall->bind_param("sssiissii",$currentDay,$currentTime,$supplierName,$_POST['itemID'],$_POST['projectID'],$_POST['phaseName'],
$_POST['taskName'],$_POST['quantity'],$amountRemaining);
$addQuotationCall->execute();
$result = $addQuotationCall->get_result();
if($addQuotationCall->affected_rows===1){
  echo "Quotation has been saved.";
  echo "<a href=" . '"quotation.php"'. '"> Back</a>';
}
else{
  echo "Error";
  echo $addQuotationCall->error;
  echo "<a href=" . '"quotation.php"'. '"> Back</a>';
}

$itemQuantityInDollarsCall->close();
$addQuotationCall->close();
?>
