# Housing Equipment Information System
This is a relational database application system for a fictional housing construction company named Damavand. 

This application provides a form-based user interface for CRUD operations to manage the different projects, phases, tasks, contracts, quotations & payments of the company.

The conceptual design of the database used in this project was done for the completion of a database course in college.

## Technologies used:
* PHP (no frameworks)
* Bootstrap 4
* XAMPP
* MySQL
* Putty

## How to run
* Make sure you have XAMPP installed on your local machine
* Clone the repository inside /xampp/htdocs folder
* Modify the config.php file with your own credentials
* Start Apache & MYSQL from XAMPP
* Access http://localhost/ConstructionInfoSys/ via your browser