<?php
 	include 'config.php';

	if($_SERVER["REQUEST_METHOD"] == "POST"){

    $addPhaseCall=$connection->prepare("INSERT INTO Phase (phaseName, projectID, estmCost, actualCost, estmTime, actualTime) VALUES(?,?,?,?,?,?)");
    $addPhaseCall->bind_param("siddii",$_POST['phaseName'],$_POST['projectID'],$_POST['estmCost'],$_POST['actualCost'],$_POST['estmTime'],$_POST['actualTime']);
    $addPhaseCall->execute();
    $result = $addPhaseCall->get_result();

		if($addPhaseCall->affected_rows===1){
      echo "Phase has been saved.";
      echo "<a href=" . '"phases.php"'. '"> Back</a>';
    }
		else{
      echo "Error";
      echo "<a href=" . '"phases.php"'. '"> Back</a>';
		}
	}
  $addPhaseCall->close();
?>
