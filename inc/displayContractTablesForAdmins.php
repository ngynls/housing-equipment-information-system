<?php
$getContractsCall=$connection->prepare("SELECT * FROM Contract as c INNER JOIN ContractDetailed as cd ON c.contractNumber=cd.contractNumber");
$getContractsCall->execute();
$result = $getContractsCall->get_result();
echo "<div class='table-responsive-sm'>";
echo "<table id='all-contracts' class='table table-striped'>";
echo "<tr>
  <th>Contract #</th>
  <th>Project ID</th>
  <th>User ID</th>
  <th>Type of contract</th>
  <th>Date of contract</th>
  <th>Cost</th>
  <th>Delete</th>
  </tr>";
  while($row=mysqli_fetch_assoc($result)){
    echo "<tr>";
    echo "<td>".$row['contractNumber']."</td>";
    echo "<td>".$row['projectID']."</td>";
    echo "<td>".$row['userID']."</td>";
    echo "<td>".$row['typeOfContract']."</td>";
    echo "<td>".$row['dateOfContract']."</td>";
    echo "<td>".$row['cost']."</td>";
    echo "<td> <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteModal".$row['contractNumber']."'>Delete</button> </td>";
    echo "</tr>";
?>
<!--Delete Modal -->
<div class="modal fade" id="deleteModal<?php echo $row['contractNumber'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete a Contract <br/> <?php echo "[".$row['contractNumber']."]";?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this contract? </p>
        <form method="post" action="deleteContract.php?contractNumber=<?php echo $row['contractNumber']; ?>">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Confirm</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--End Delete Modal -->
<?php
  }
    $getContractsCall->close();
    echo "</table>";
    echo "</div>";

?>
