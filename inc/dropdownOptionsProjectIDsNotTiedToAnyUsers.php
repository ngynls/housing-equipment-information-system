<select class="form-control" name="projectID" required>
<?php
$getProjectIDs=$connection->prepare("SELECT p.projectID, p.name FROM project as p LEFT JOIN contract as c ON p.projectID=c.projectID WHERE c.projectID is null");
$getProjectIDs->execute();
$results = $getProjectIDs->get_result();
  while($row=mysqli_fetch_assoc($results)){
    echo "<option value=".$row['projectID'].">".$row['projectID']." [".$row['name']."]</option>";
  }
$getProjectIDs->close();
?>
</select>
