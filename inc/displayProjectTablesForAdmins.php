<?php
  //$query="SELECT p.projectID, p.name, p.type, p.currentStatus, c.userID FROM project as p, contract as c WHERE p.projectID=c.projectID";
  $query="SELECT projectID, name, type, currentStatus FROM project";
  $result = mysqli_query($connection,$query);
  echo "<div class='table-responsive-sm'>";
  echo "<table id='all-projects' class='table table-striped'>";
  echo "<tr>
    <th>Project Name</th>
    <th>Project Type</th>
    <th>Current Status</th>
    <th>Update</th>
    <th>Delete</th>
    </tr>";
    while($row=mysqli_fetch_assoc($result)){
      echo "<tr>";
      echo "<td>".$row['name']."</td>";
      //echo "<td>".$row['userID']."</td>";
      echo "<td>".$row['type']."</td>";
      echo "<td>".$row['currentStatus']."</td>";
      echo "<td> <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#updateModal".$row['projectID']."'>Update</button> </td>";
      echo "<td> <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteModal".$row['projectID']."'>Delete</button> </td>";
      echo "</tr>";
  ?>
  <!--Update Modal -->
  <div class="modal fade" id="updateModal<?php echo $row['projectID'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update a Project <br/> <?php echo "[".$row['name']."]";?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="updateProject.php?id=<?php echo $row['projectID']; ?>">
                <div class="form-group">
                  <label for="input-projectName">Project name</label>
                  <input type="text" class="form-control" name="projectName" value="<?php echo $row['name'] ?>" placeholder="Enter the project name">
                </div>
                <div class="form-group">
                  <label for="input-type">Type</label>
                  <select class="form-control" name ="type">
                    <option value="residential">residential</option>
                    <option value="industrial">industrial</option>
                    <option value="commercial">commercial</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="input-type">Current status</label>
                  <select class="form-control" name ="currentStatus">
                    <option value="incomplete">incomplete</option>
                    <option value="complete">complete</option>
                  </select>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--End Update Modal -->
  <!--Delete Modal -->
  <div class="modal fade" id="deleteModal<?php echo $row['projectID'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete a Project <br/> <?php echo "[".$row['name']."]";?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this project? </p>
          <form method="post" action="deleteProject.php?id=<?php echo $row['projectID']; ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--End Delete Modal -->
<?php
    }
    echo "</table>";
    echo "</div>";
?>
