<!--Card section for clients only -->
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col md-6">
      <!--Project card -->
       <div class="card">
          <div class="card-body">
             <h5 class="card-title">Projects</h5>
          </div>
      </div>
      <!--END Project card -->
      </div>
      <div class="col md-6">
        <!--Phases card -->
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Phases</h5>
          </div>
        </div>
        <!--Phases card -->
      </div>
    </div>
  </div>
</div>
<!--End Card section for clients only -->
