<!--Card section for admin only -->
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col md-6 xs-12">
      <!--Project card -->
       <div class="card">
          <div class="card-body">
             <h5 class="card-title">Projects</h5>
             <?php
                include "displayProjectCount.php";
              ?>
          </div>
          <div class="card-footer">
            <a href="projects.php">
            View details
            <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
      </div>
      <!--END Project card -->
      </div>
      <div class="col md-6 xs-12">
        <!--Phases card -->
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Phases</h5>
            <?php
               include "displayPhaseCount.php";
             ?>
          </div>
          <div class="card-footer">
            <a href="phases.php">
            View details
            <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!--Phases card -->
      </div>
      <div class="col md-12 xs-12">
        <!--Task card -->
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Tasks</h5>
            <?php
               include "displayTaskCount.php";
             ?>
          </div>
          <div class="card-footer">
            <a href="tasks.php">
            View details
            <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!--Task card -->
      </div>
    </div>
  </div>
</div>
<!--End Card section for admin only -->
