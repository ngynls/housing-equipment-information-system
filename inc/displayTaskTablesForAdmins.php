<?php
$sqlCall=$connection->prepare("SELECT * FROM Task");
$sqlCall->execute();
$result = $sqlCall->get_result();
echo "<div class='table-responsive-sm'>";
echo "<table id='all-phases' class='table table-striped'>";
echo "<tr>
  <th>Task Name</th>
  <th>Task Date</th>
  <th>Project ID</th>
  <th>Phase Name</th>
  <th>Task Cost</th>
  <th>Time (in hrs)</th>
  <th>Update</th>
  <th>Delete</th>
  </tr>";
  while($row=mysqli_fetch_assoc($result)){
    echo "<tr>";
    echo "<td>".$row['taskName']."</td>";
    echo "<td>".$row['taskDate']."</td>";
    echo "<td>".$row['projectID']."</td>";
    echo "<td>".$row['phaseName']."</td>";
    echo "<td>".$row['taskCost']."</td>";
    echo "<td>".$row['time']."</td>";
    echo "<td> <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#updateModal".seoUrl($row['taskName']).seoUrl($row['phaseName']).$row['projectID']."'>Update</button> </td>";
    echo "<td> <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteModal".seoUrl($row['taskName']).seoUrl($row['phaseName']).$row['projectID']."'>Delete</button> </td>";
    echo "</tr>";
?>
<!--localhost/ConstructionInfoSys/updateTask.php?taskName=blah&phaseName=blah&id=1-->
<!--Update Modal -->
<div class="modal fade" id="updateModal<?php echo seoUrl($row['taskName']).seoUrl($row['phaseName']).$row['projectID'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update a Task <br/> <?php echo "[".$row['taskName']."]";?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="updateTask.php?taskName=<?php echo seoUrl($row['taskName'])."&phaseName=".seoUrl($row['phaseName'])."&id=".$row['projectID']; ?>">
          <div class="form-group">
            <label for="input-taskName">Task name</label>
            <input type="text" class="form-control" name="taskName" value="<?php echo $row['taskName'] ?>" placeholder="Enter the task name" required>
          </div>
          <div class="form-group">
            <label for="input-taskDate">Task date</label>
            <input type="date" class="form-control" name="taskDate" value="<?php echo $row['taskDate'] ?>" placeholder="Enter the task date" required>
          </div>
          <div class="form-group">
            <label for="input-projectID">Project ID</label>
            <input type="text" class="form-control" name="projectID" value="<?php echo $row['projectID'] ?>" placeholder="Enter the project ID" required>
          </div>
          <div class="form-group">
            <label for="input-phaseName">Phase Name</label>
            <input type="text" class="form-control" name="phaseName" value="<?php echo $row['phaseName'] ?>" placeholder="Enter the phase name" required>
          </div>
          <div class="form-group">
            <label for="input-taskCost">Task cost</label>
            <input type="text" class="form-control" name="taskCost" value="<?php echo $row['taskCost'] ?>" placeholder="Enter the task cost (in $, two decimals)" required>
          </div>
          <div class="form-group">
            <label for="input-time">Time</label>
            <input type="text" class="form-control" name="time" value="<?php echo $row['time'] ?>" placeholder="Enter the actual time (in h)" required>
          </div>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--End Update Modal -->
<!--localhost/ConstructionInfoSys/deleteTask.php?taskName=blah&phaseName=blah&id=1-->
<!--Delete Modal -->
<div class="modal fade" id="deleteModal<?php echo seoUrl($row['taskName']).seoUrl($row['phaseName']).$row['projectID'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete a Task <br/> <?php echo "[".$row['taskName']."]";?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this task? </p>
        <form method="post" action="deleteTask.php?taskName=<?php echo seoUrl($row['taskName'])."&phaseName=".seoUrl($row['phaseName'])."&id=".$row['projectID']; ?>">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Confirm</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--End Delete Modal -->
<?php
  }
    $sqlCall->close();
    echo "</table>";
    echo "</div>";

    function seoUrl($string) {
      $string = strtolower($string);
      $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
      $string = preg_replace("/[-]+/", "_", $string);
      $string = preg_replace("/[\s]/", "-", $string);
      return $string;
    }
?>
