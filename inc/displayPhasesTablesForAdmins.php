<?php
$sqlCall=$connection->prepare("SELECT phaseName, projectID, estmCost, actualCost, estmTime, actualTime FROM Phase");
$sqlCall->execute();
$result = $sqlCall->get_result();
echo "<div class='table-responsive-sm'>";
echo "<table id='all-phases' class='table table-striped'>";
echo "<tr>
  <th>Phase Name</th>
  <th>Project ID</th>
  <th>Estimated cost</th>
  <th>Actual cost</th>
  <th>Estimated time</th>
  <th>Actual time</th>
  <th>Update</th>
  <th>Delete</th>
  </tr>";
  while($row=mysqli_fetch_assoc($result)){
    echo "<tr>";
    echo "<td>".$row['phaseName']."</td>";
    echo "<td>".$row['projectID']."</td>";
    echo "<td>".$row['estmCost']."</td>";
    echo "<td>".$row['actualCost']."</td>";
    echo "<td>".$row['estmTime']."</td>";
    echo "<td>".$row['actualTime']."</td>";
    echo "<td> <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#updateModal".seoUrl($row['phaseName']).$row['projectID']."'>Update</button> </td>";
    echo "<td> <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteModal".seoUrl($row['phaseName']).$row['projectID']."'>Delete</button> </td>";
    echo "</tr>";
?>
<!--localhost/ConstructionInfoSys/updatePhase.php?phaseName=blah&id=1-->
<!--Update Modal -->
<div class="modal fade" id="updateModal<?php echo seoUrl($row['phaseName']).$row['projectID'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update a Phase <br/> <?php echo "[".$row['phaseName']."]";?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="updatePhase.php?phaseName=<?php echo seoUrl($row['phaseName'])."&id=".$row['projectID']; ?>">
          <div class="form-group">
            <label for="input-phaseName">Phase name</label>
            <input type="text" class="form-control" name="phaseName" value="<?php echo $row['phaseName'] ?>" placeholder="Enter the phase name" required>
          </div>
          <div class="form-group">
            <label for="input-projectID">Project ID</label>
            <input type="text" class="form-control" name="projectID" value="<?php echo $row['projectID'] ?>" placeholder="Enter the project ID" required>
          </div>
          <div class="form-group">
            <label for="input-estmCost">Estimate cost</label>
            <input type="text" class="form-control" name="estmCost" value="<?php echo $row['estmCost'] ?>" placeholder="Enter the estimate cost (two decimals, no $)" required>
          </div>
          <div class="form-group">
            <label for="input-actualCost">Actual cost</label>
            <input type="text" class="form-control" name="actualCost" value="<?php echo $row['actualCost'] ?>" placeholder="Enter the actual cost (two decimals, no $)" required>
          </div>
          <div class="form-group">
            <label for="input-estmTime">Estimate time</label>
            <input type="text" class="form-control" name="estmTime" value="<?php echo $row['estmTime'] ?>" placeholder="Enter the estimate time (in h)" required>
          </div>
          <div class="form-group">
            <label for="input-actualTime">Actual time</label>
            <input type="text" class="form-control" name="actualTime" value="<?php echo $row['actualTime'] ?>" placeholder="Enter the actual time (in h)" required>
          </div>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--End Update Modal -->
<!--localhost/ConstructionInfoSys/deletePhase.php?phaseName=blah&id=1-->
<!--Delete Modal -->
<div class="modal fade" id="deleteModal<?php echo seoUrl($row['phaseName']).$row['projectID'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete a Phase <br/> <?php echo "[".$row['phaseName']."]";?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this phase? </p>
        <form method="post" action="deletePhase.php?phaseName=<?php echo seoUrl($row['phaseName'])."&id=".$row['projectID']; ?>">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Confirm</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--End Delete Modal -->
<?php
} //end of white loop
    $sqlCall->close();
    echo "</table>";
    echo "</div>";

    function seoUrl($string) {
      $string = strtolower($string);
      $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
      $string = preg_replace("/[-]+/", "_", $string);
      $string = preg_replace("/[\s]/", "-", $string);
      return $string;
    }

?>
