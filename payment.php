<?php
	session_start();
// retrieve the login_user from session.php so that the user can access multiple pages
	if (isset($_SESSION['login_user']) ) {

		$loginSuccess = true;
	}
	else {
		$loginSuccess = false;
	}
?>

<?php

	if (isset($_POST['phaseName']) && isset($_POST['taskName']) && isset($_POST['dateOfQuote'])
		&& isset($_POST['timeOfQuote']) && isset($_POST['itemName']) && isset($_POST['supplierName'])
		&& isset($_POST['paymentAmount']) && isset($_POST['projectID'])) {
			include 'config.php';

			$projectID=$_POST['projectID'];
			$phaseName=$_POST['phaseName'];
			$taskName=$_POST['taskName'];
			$dateOfQuote=$_POST['dateOfQuote'];
			$timeOfQuote=$_POST['timeOfQuote'];
			$itemName=$_POST['itemName'];
			$supplierName=$_POST['supplierName'];
			$amountPay=$_POST['paymentAmount'];

			$qryItem = "select i.itemID from Item as i where i.itemName='$itemName' and i.supplierName='$supplierName'";
			$resultItem=mysqli_query($connection,$qryItem);

			$row=mysqli_fetch_assoc($resultItem);
			$itemID=$row['itemID'];

			//check if amountPay-amountRemaining>0
			$amountRemainingQuery= "select amountRemaining from Quotation as q
			where q.dateOfQuote='$dateOfQuote' and q.timeOfQuote='$timeOfQuote' and supplierName='$supplierName' and projectID='$projectID'
			and phaseName='$phaseName' and taskName='$taskName'";
			$resultAmountRemainingQuery=mysqli_query($connection,$amountRemainingQuery);
			$rowAmountRemaining=mysqli_fetch_assoc($resultAmountRemainingQuery);
			$amountRemaining=$rowAmountRemaining['amountRemaining'];

			if($amountRemaining-$amountPay>=0){
				$currentTime=date("Y-m-d H:i:s");
				$insert="insert into Payment (amountPay, dateOfQuote, timeOfQuote, timeOfPayment, supplierName, itemID, projectID, phaseName, taskName)
					values('$amountPay', '$dateOfQuote', '$timeOfQuote', '$currentTime', '$supplierName', '$itemID', '$projectID', '$phaseName', '$taskName')";
				$insertResult=mysqli_query($connection,$insert);
				if ($insertResult) {
					echo "Payment succeeded";
					echo "<a href=" . '"quotation.php"'. '"> Back</a>';
				}
				else {
					echo "Payment failed";
					echo "<a href=" . '"quotation.php"'. '"> Back</a>';
				}
			}
			else{
				echo "Payment failed. The amount paid exceeded the amount remaining";
				echo "<a href=" . '"quotation.php"'. '"> Back</a>';
			}
		}
?>
<html>
