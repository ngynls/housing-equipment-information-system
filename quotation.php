<?php
	session_start();
// retrieve the login_user from session.php so that the user can access multiple pages
	if (isset($_SESSION['login_user']) ) {

		$loginSuccess = true;
	}
	else {
		$loginSuccess = false;
	}
?>
<html>
<!--HEAD-->
<head>
  <meta charset = "utf-8" />
	<link rel="stylesheet" type="text/css" href="css/dashboard.css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
	<title>Damavand Housing and Condo Company Information System- [Dashboard]</title>
</head>
<!-- END HEAD-->
<!--- BODY ----->
<body>

  <!--Navbar -->
  <div class="navigationBar">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Damavand Information System</a>
      <!--Toggler-->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapsable" aria-controls="navbarCollapsable" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!--END Toggler-->
      <!--Collapsable nav -->
      <div class="collapse navbar-collapse" id="navbarCollapsable">
         <a class="nav-link" href="myDashboard.php"><i class="fas fa-tachometer-alt"></i>  Dashboard</a>
         <?php
           if ($_SESSION['WhetherAdmin'] == 'True')
           {
             echo "<a class=". '"nav-link"' . "href=" . '"projects.php"' . "><i class=" .'"fas fa-project-diagram"'."></i>  Project</a>";
						 echo "<a class=". '"nav-link"' . "href=" . '"contracts.php"' . "><i class=".'"fas fa-scroll"'."></i>  Contract</a>";
             echo "<a class=". '"nav-link"' . "href=" . '"phases.php"' . "><i class=".'"fas fa-wrench"'."></i>  Phase</a>";
						 echo "<a class=". '"nav-link"' . "href=" . '"tasks.php"' . "><i class=".'"fas fa-wrench"'."></i>  Tasks</a>";
             echo "<a class=" . '"nav-link"' . "href=" . '"reportAdmin.php"' . "><i class=" . '"fas fa-file"' ."></i>  Reports</a>";
             echo "<a class=" . '"nav-link"' . "href=" . '"quotation.php"' . "><i class=" . '"fas fa-file-invoice-dollar"' ."></i>  Quotations</a>";
           }
           else
           {
             echo "<a class=" . '"nav-link"' ."href=" .'"reportCust.php"'. ">  My Project</a>";
           }
         ?>
         <div class="navbar-text ml-auto" >
           <span class="navbar-text">
             <?php
               echo 'Welcome ' . $_SESSION['login_user'] . '<br />';
             ?>
           </span>
         </div>
         <a class="nav-link" href="logout.php"><i class="fas fa-sign-out-alt"></i>  Logout</a>
      </div>
      <!--Collapsable nav -->
    </nav>

  </div>
<!--END NavBar -->

<!--Main -->
	<div class="main">

    <!--Content -->
    <ol class="breadcrumb">
      <li>Quotations</li>
    </ol>

    <section id="main-content">
			<a href='#' data-toggle='modal' data-target='#addQuotationModal'> Add a new quotation</a>
			<?php
				include 'config.php';

				// Fetch data into the dropdown list
				$sql="select projectID, name from Project";
				$resultID = mysqli_query($connection,$sql);

				echo '<form action="" method="POST">';
				echo "<select name='optionName'>";
				while ($row = mysqli_fetch_assoc($resultID))
				{
						unset($id, $name);

						$id = $row['projectID'];
						$name = $row['name'];
						echo '<option value="'.$id.'">'.$name.'</option>';

				}

				echo "</select>";
				echo "<input name='submit' type='submit' value='Submit' />";
				echo "</form>";


				// Validate the dropdown list selection (which project) and print the reports
				if($_SERVER['REQUEST_METHOD'] =='POST')
				{
					$optionName=$_POST['optionName'];
					$qry = "select q.dateOfQuote, q.timeOfQuote, q.supplierName, q.itemID, i.itemName, q.phaseName, q.taskName, q.quantity, q.amountRemaining ".
							"from Quotation as q, Item as i " .
							"where q.projectID = '$optionName' and q.itemID=i.itemID";
					$result = mysqli_query($connection,$qry);

					echo "<p style='color:blue; font-size: 20px;'  >Quotations Review</p>";
					echo "<div class='table-responsive-sm'>";
					echo "<table class='table table-striped' style='border-collapse: collapse'>";
					echo "<tr>
						<th>Phase</th>
						<th>Task</th>
						<th>Date</th>
						<th>Time</th>
						<th>Item Name</th>
						<th>Supplier</th>
						<th>Quantity</th>
						<th>Amount remaining</th>
						</tr>";

					while($row=mysqli_fetch_assoc($result)){
						echo "<tr class=" ."'quoteHeader'" .">";
						echo "<td>".$row['phaseName']."</td>";
						echo "<td>".$row['taskName']."</td>";
						echo "<td>".$row['dateOfQuote']."</td>";
						echo "<td>".$row['timeOfQuote']."</td>";
						echo "<td>".$row['itemName']."</td>";
						echo "<td>".$row['supplierName']."</td>";
						echo "<td>".$row['quantity']."</td>";
						echo "<td>".$row['amountRemaining']."</td>";
						$phaseName=$row['phaseName'];
						$taskName=$row['taskName'];
						$dateOfQuote=$row['dateOfQuote'];
						$timeOfQuote=$row['timeOfQuote'];
						$itemName=$row['itemName'];
						$supplierName=$row['supplierName'];


						if ($row['amountRemaining'] > 0) {
							echo "<td><form action=" .'"payment.php"'. " method=" .'"post"' . ">";
							echo "<input type=" .'"hidden"' ." name=" .'"projectID"'." value=" .'"' .$optionName .'">';
							echo "<input type=" .'"hidden"' ." name=" .'"phaseName"'." value=" .'"' .$phaseName . '">';
							echo "<input type=" .'"hidden"' ." name=" .'"taskName"'." value=" .'"' .$taskName . '">';
							echo "<input type=" .'"hidden"' ." name=" .'"dateOfQuote"'." value=" .'"' .$dateOfQuote . '">';
							echo "<input type=" .'"hidden"' ." name=" .'"timeOfQuote"'." value=" .'"' .$timeOfQuote . '">';
							echo "<input type=" .'"hidden"' ." name=" .'"itemName"'." value=" .'"' .$itemName . '">';
							echo "<input type=" .'"hidden"' ." name=" .'"supplierName"'." value=". '"' .$supplierName.'">';
							echo "<input type=" .'"text"' . " name=" .'"paymentAmount"'. ">";
							echo "<input type=" .'"submit"' . " value=" .'"make a payment"'. " name=" .'"makePayment"'. ">";
							echo "</form></td>";
							echo "</tr>";
						}
						$qryPayment = "Select distinct p.paymentID, p.amountPay, p.timeOfPayment ".
						"from Payment as p, Quotation as q ".
						"where p.projectID = '$optionName' and p.phaseName='$phaseName' and p.taskName='$taskName' and p.dateOfQuote='$dateOfQuote' ".
						"and p.timeOfQuote='$timeOfQuote' and p.supplierName='$supplierName'";
						$resultPayment = mysqli_query($connection,$qryPayment);

						echo "<tr><th>Payment ID</th><th>Amount paid</th><th>Time of Payment</th></tr>";
						while($rowPayment=mysqli_fetch_assoc($resultPayment)) {
							echo "<tr>";
							echo "<td>".$rowPayment['paymentID']."</td>";
							echo "<td>".$rowPayment['amountPay']."</td>";
							echo "<td>".$rowPayment['timeOfPayment']."</td>";
							echo "</tr>";
						}
					}
					echo "</table>";
					echo "</div>";
				}


			?>
    </section>
    <!--END Content -->

		<!--Add Quotation Modal -->
		<div class="modal fade" id="addQuotationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add a new quotation </h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form method="post" action="addQuotation.php">
									<div class="form-group">
										<label for="input-itemID">Item ID</label>
										<?php
											include "inc/dropdownOptionsOnlyExistingItemID.php";
										 ?>
									</div>
									<div class="form-group">
										<label for="input-projectID">Project ID</label>
										<?php
											include "inc/dropdownOptionsOnlyExistingProjectIDs.php";
										?>
									</div>
									<div class="form-group">
										<label for="input-phaseName">Phase Name</label>
										<input type="text" class="form-control" name="phaseName" placeholder="Enter the phase name" required>
									</div>
									<div class="form-group">
										<label for="input-taskName">Task Name</label>
										<input type="text" class="form-control" name="taskName" placeholder="Enter the task name" required>
									</div>
									<div class="form-group">
										<label for="input-quantity">Quantity</label>
										<input type="text" class="form-control" name="quantity" placeholder="Enter the quantity" required>
									</div>
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save changes</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--End Add Quotation Modal -->

	</div>
<!--END Main -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
<!--- END BODY ----->
</html>
