<?php
include 'config.php';

if($_SERVER["REQUEST_METHOD"] == "POST"){

  $oldTaskName=seoUrlReverse($_GET['taskName']);
  $oldPhaseName=seoUrlReverse($_GET['phaseName']);
  $oldID=$_GET['id'];

  $updateTaskCall=$connection->prepare("UPDATE Task SET taskName= ?, taskDate= ?, projectID= ? , phaseName= ? , taskCost= ?, time= ?
  WHERE taskName='$oldTaskName' AND phaseName='$oldPhaseName' AND projectID='$oldID'");
  $updateTaskCall->bind_param("ssisdi",$_POST['taskName'],$_POST['taskDate'],$_POST['projectID'],$_POST['phaseName'],$_POST['taskCost'],$_POST['time']);
  $updateTaskCall->execute();

  if($updateTaskCall->affected_rows===1){
    echo "Task has been updated.";
    echo "<a href=" . '"tasks.php"'. '"> Back</a>';
  }
  else{
    echo "Error";
    echo "<a href=" . '"tasks.php"'. '"> Back</a>';
  }
}
$updateTaskCall->close();

function seoUrlReverse($string){
$string = preg_replace("/[-]/", " ", $string);
$string = preg_replace("/[_]+/", "-", $string);
$string=ucwords($string);
return $string;
}

?>
