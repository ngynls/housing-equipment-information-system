<?php
include 'config.php';
if($_SERVER["REQUEST_METHOD"] == "POST"){
  $formattedTaskName=seoUrlReverse($_GET['taskName']);
  $deleteTaskCall=$connection->prepare("DELETE FROM Task WHERE taskName= ? AND phaseName= ? AND projectID= ?");
  $deleteTaskCall->bind_param("ssi", $formattedTaskName, $_GET['phaseName'], $_GET['id']);
  $deleteTaskCall->execute();

  if($deleteTaskCall->affected_rows===1){
    echo "Task has been deleted successfully.";
    echo "<a href=" . '"tasks.php"'. '"> Back</a>';
  }
  else{
    echo "Error";
    echo "<a href=" . '"tasks.php"'. '"> Back</a>';
  }
}
$deleteTaskCall->close();

function seoUrlReverse($string){
$string = preg_replace("/[-]/", " ", $string);
$string = preg_replace("/[_]+/", "-", $string);
$string=ucwords($string);
return $string;
}

?>
