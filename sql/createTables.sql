CREATE TABLE Users (
UserID varchar(200),
Password varchar(255),
PhoneNumber char(14),
Address varchar(255),
FirstName varchar(255),
LastName varchar(255),
WhetherAdmin enum('True', 'False'),
PRIMARY KEY (UserID)
);

create table Project(
projectID INT(11) PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(200),
type ENUM('residential','industrial','commercial'),
currentStatus VARCHAR(200)
);

CREATE TABLE Supplier (
Name varchar(255) PRIMARY KEY,
Address varchar(255)
);
///////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE TABLE ContractDetailed(
contractNumber int(11) AUTO_INCREMENT,
typeOfContract enum('lump sum', 'cost plus', 't&m', 'unit pricing'),
dateOfContract date,
cost decimal(12,2),
PRIMARY KEY(contractNumber),
);

CREATE TABLE Contract(
contractNumber int(11),
projectID int(11),
userID varchar(200),
PRIMARY KEY (contractNumber, projectID, userID),
FOREIGN KEY (contractNumber) REFERENCES ContractDetailed (contractNumber) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (projectID) REFERENCES Project (projectID) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (userID) REFERENCES Users (UserID) ON DELETE CASCADE ON UPDATE CASCADE
);


///////////////////////////////////////////////////////////////////////////////////////////////////////////
CREATE TABLE Phase ( 
phaseName varchar(200), 
projectID int(11), 
estmCost decimal(20,2), 
actualCost decimal(20,2), 
estmTime int(11), 
actualTime int(11),
PRIMARY KEY(phaseName, projectID),
FOREIGN KEY (projectID) REFERENCES Project(projectID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Task ( 
taskName varchar(200), 
taskDate date, 
projectID int(11), 
phaseName varchar(200), 
taskCost decimal(20, 2), 
time int(11), 
PRIMARY KEY ( taskName, phaseName, projectID ), 
FOREIGN KEY ( phaseName ) REFERENCES Phase(phaseName) ON DELETE CASCADE ON UPDATE CASCADE, 
FOREIGN KEY ( projectID ) REFERENCES Project(projectID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Item(
supplierName varchar(255),
itemID int(11),
itemName varchar(200),
price decimal(20,2),
PRIMARY KEY(supplierName, itemID),
FOREIGN KEY (supplierName) REFERENCES Supplier(Name) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Quotation(
dateOfQuote date,
timeOfQuote time,
supplierName varchar(255),
itemID int(11),
projectID int(11),
phaseName varchar(200),
taskName varchar(200),
quantity int,
amountRemaining decimal(20,2),
PRIMARY KEY(dateOfQuote,timeOfQuote,supplierName, itemID, projectID, phaseName, taskName),
FOREIGN KEY (supplierName, itemID) REFERENCES Item(supplierName, itemID) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (projectID) REFERENCES Project(projectID) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (phaseName) REFERENCES Phase(phaseName) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (taskName) REFERENCES Task(taskName) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Payment(
paymentID int(11) AUTO_INCREMENT,
amountPay decimal(20,2),
dateOfQuote date,
timeOfQuote time,
timeOfPayment datetime,
supplierName varchar(255),
itemID int(11),
projectID int(11),
phaseName varchar(200),
taskName varchar(200),
PRIMARY KEY (paymentID, dateOfQuote, timeOfQuote, timeOfPayment,supplierName, itemID, projectID, phaseName, taskName),
FOREIGN KEY (dateOfQuote, timeOfQuote, supplierName, itemID, projectID, phaseName, taskName) REFERENCES Quotation(dateOfQuote,timeOfQuote,supplierName, itemID, projectID, phaseName, taskName) ON DELETE CASCADE ON UPDATE CASCADE
);
