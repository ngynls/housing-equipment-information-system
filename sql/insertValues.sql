INSERT INTO Users  VALUES ('jsmith320', 'baddabing','514-123-4321', '123 Fake Avenue','John', 'Smith', 'False');
INSERT INTO Users VALUES ( 'janedoe111', 'helloworld', '514-436-1111', '456 Pine Road', 'Jane', 'Doe', 'False');
INSERT INTO Users VALUES ( 'icam1234', 'diddlydoo', '514-222-2222', '1001 Cake Avenue', 'Ian', 'Cameron', 'False');
INSERT INTO Users VALUES ( 'admin346', 'protectyourphone', '438-112-4321', '445 Network Road, Montreal', 'Mr', 'Admin', 'True');
INSERT INTO Users VALUES ( 'joeyjoe444', 'itsafakename', '438-453-4433', '2222 Hill Avenue, Montreal','Joey', 'JoeJoe', 'False' );
INSERT INTO Users VALUES ( 'jholly42', 'isasecretspy', '438-911-9111', '2223 NSA Road, St. Lambert', 'Jennifer', 'Holly', 'False' );
INSERT INTO Users VALUES ('branders999', 'donttrusthim', '438-911-9333', '1263 Secret Road, Westmount','Brandon', 'Faker', 'False' );
INSERT INTO Users VALUES ( 'startrek2', 'Startrekgeek','438-955-1122', '5555 Space Road, Montreal', 'Jean-Luc', 'Picard', 'False' );
INSERT INTO Users VALUES ( 'starwarsguy6', 'darkside', '438-766-2364', '777 Death Star, Montreal', 'Darth','Vader', 'False' );
INSERT INTO Users VALUES ( 'logan420', 'adamantanium', '514-123-0987', '4444 Xavier, Montreal','Logan', 'Wolverine', 'False' );
INSERT INTO Users VALUES ( 'jcleese132', 'montypython', '514-276-5467', '4334 Circus, Montreal', 'John', 'Cleese', 'False' );

INSERT INTO Project VALUES ( 1, 'Dragon House', 'residential', 'incomplete' );
INSERT INTO Project VALUES ( 2, 'Tile Factory', 'industrial', 'incomplete' );
INSERT INTO Project VALUES (3,'River City Condominium','residential','complete');
INSERT INTO Project VALUES (4,'Fashion Atelier','commercial','incomplete');
INSERT INTO Project VALUES (5, 'Blackwood Warehouse','industrial','incomplete');
INSERT INTO Project VALUES (6, 'Lord of the Fries', 'commercial', 'incomplete');
INSERT INTO Project VALUES (7, 'Habitat 68', 'residential', 'complete');
INSERT INTO Project VALUES (8, 'Glass Manufacturing Plant', 'industrial','incomplete');
INSERT INTO Project VALUES (9, 'X-Mansion','residential','complete');
INSERT INTO Project VALUES (10, 'Starlight Comix' , 'commercial', 'incomplete');

INSERT INTO Supplier VALUES ( 'SmartElectric', '222 Electrify Avenue' );
INSERT INTO Supplier VALUES ( 'Wood Chippers', '543 Forest Road' );
INSERT INTO Supplier VALUES ( 'Cheapo Concrete', '667 Block Road' );
INSERT INTO Supplier VALUES ( 'Light Bright', '555 Darkness Avenue' );
INSERT INTO Supplier VALUES ( 'Plumbing World', '1234 Sewage Road' );
INSERT INTO Supplier VALUES ( 'Sturdy Metals', '6599 Terminator Avenue' );
INSERT INTO Supplier VALUES ( 'Tractor Rent Depot', '1555 MorePower' );
INSERT INTO Supplier VALUES ( 'Ventilation Dudes', '6511 SweetAir Avenue' );
INSERT INTO Supplier VALUES ( 'Single Shingle', '222 Roofers Road' );
INSERT INTO Supplier VALUES ( 'Tool Shed', '7777 Hardware Avenue' );

INSERT INTO Contract VALUES ( 100, 1, 'jsmith320' );
INSERT INTO Contract VALUES ( 101, 2, 'janedoe111' );
INSERT INTO Contract VALUES ( 102, 3, 'Icam1234');
INSERT INTO Contract VALUES ( 103, 4, 'branders999');
INSERT INTO Contract VALUES ( 104, 5, 'jcleese132');
INSERT INTO Contract VALUES ( 105, 6, 'jholly42');
INSERT INTO Contract VALUES ( 106, 7, 'joeyjoe444');
INSERT INTO Contract VALUES ( 107, 8, 'logan420');
INSERT INTO Contract VALUES ( 108, 9, 'startrek2');
INSERT INTO Contract VALUES ( 109, 10, 'starwarsguy6');

INSERT INTO ContractDetailed VALUES ( 100,'lump sum', '2016-12-21', 125000.00 );
INSERT INTO ContractDetailed VALUES ( 101, 'cost plus', '2017-02-12', 90750.00 );
INSERT INTO ContractDetailed VALUES ( 102, 't&m', '2016-11-20', 525000.00);
INSERT INTO ContractDetailed VALUES ( 103, 'cost plus', '2016-09-10', 250000.00);
INSERT INTO ContractDetailed VALUES ( 104, 'unit pricing', '2017-02-11', 300000.00);
INSERT INTO ContractDetailed VALUES ( 105, 'lump sum', '2017-01-05', 750000.00);
INSERT INTO ContractDetailed VALUES ( 106, 'unit pricing', '2016-08-25', 575000.00);
INSERT INTO ContractDetailed VALUES ( 107, 'lump sum', '2016-07-27', 1250000.00);
INSERT INTO ContractDetailed VALUES ( 108, 't&m', '2017-01-03', 1500000.00);
INSERT INTO ContractDetailed VALUES ( 109, 'cost plus', '2017-03-24', 2000000.00);

INSERT INTO Phase  VALUES ( 'Design and Planning', 1, 25000.00, 25750.00, 15, 15);
INSERT INTO Phase VALUES ( 'Construction', 1, 100000.00, 102000.00, 60, 62 );
INSERT INTO Phase  VALUES ( 'Design and Planning', 2, 9000.00, 9000.00, 10, 10);
INSERT INTO Phase  VALUES ( 'Documentation', 2, 1000.00, 1000.00, 3, 3);
INSERT INTO Phase VALUES ( 'Construction', 2, 80750.00, 81000.00, 50, 52 );
INSERT INTO Phase VALUES ( 'Construction', 3, 450000.00, 460000.00, 85, 90 );
INSERT INTO Phase VALUES ( 'Construction', 4, 200000.00, 200000.00, 75, 75 );
INSERT INTO Phase VALUES ( 'Construction', 5, 270000.00, 275000.00, 80, 82 );
INSERT INTO Phase VALUES ( 'Construction', 6, 675000.00, 675000.00, 97, 97 );
INSERT INTO Phase VALUES ( 'Construction', 7, 525000.00, 525000.00, 50, 50 );
INSERT INTO Phase VALUES ( 'Construction', 8, 1100000.00, 1150000.00, 120, 125 );
INSERT INTO Phase VALUES ( 'Construction', 9, 1300000.00, 1300000.00, 130, 130 );
INSERT INTO Phase VALUES ( 'Construction', 10, 1700000.00, 1600000, 165, 160 );

INSERT INTO Task VALUES ( 'Flooring', '2017-01-01',1, 'Construction', 20000.00, 12 );
INSERT INTO Task VALUES ( 'Windows', '2017-01-01',1, 'Construction', 10000.00, 5 );
INSERT INTO Task VALUES ( 'Wiring', '2017-01-01',1, 'Construction', 20000.00, 7 );
INSERT INTO Task VALUES ( 'On-site Analysis', '2017-01-01',1, 'Design and Planning', 8000.00, 5 );
INSERT INTO Task VALUES ( 'Requirements', '2017-01-01', 2, 'Design and Planning', 5000.00, 5 );
INSERT INTO Task VALUES ( 'On-site Analysis', '2017-01-01', 2, 'Design and Planning', 5000.00, 5 );
INSERT INTO Task VALUES ( 'Plans and Records',  '2017-01-01', 2, 'Documentation', 1000.00, 3 );
INSERT INTO Task VALUES ( 'Gyprocs', '2017-01-01', 2, 'Construction', 10000.00, 5 );
INSERT INTO Task VALUES ( 'Permits', '2017-01-01', 3, 'Construction', 10000.00, 5 );
INSERT INTO Task VALUES ( 'Plumbing', '2017-01-01', 3, 'Construction', 50000.00, 15 );
INSERT INTO Task VALUES ( 'HVAC', '2017-01-01', 4, 'Construction', 30000.00, 10 );
INSERT INTO Task VALUES ( 'Flooring', '2017-01-01', 4, 'Construction', 40000.00, 18 );
INSERT INTO Task VALUES ( 'Wiring', '2017-01-01', 5, 'Construction', 22500.00, 10 );
INSERT INTO Task VALUES ( 'Plumbing', '2017-01-01', 5, 'Construction', 37500.00, 15 );
INSERT INTO Task VALUES ( 'Lighting', '2017-01-01', 6, 'Construction', 15000.00, 10 );
INSERT INTO Task VALUES ( 'Foundation', '2017-01-01', 7, 'Construction', 50000.00, 10 );
INSERT INTO Task VALUES ( 'Excavation', '2017-01-01', 8, 'Construction', 65000.00, 10 );
INSERT INTO Task VALUES ( 'Carpentry', '2017-01-01', 9, 'Construction', 72500.00, 15 );
INSERT INTO Task VALUES ( 'Roofing', '2017-01-01', 10, 'Construction', 100000.00, 10 );
INSERT INTO Task VALUES ( 'Painting', '2017-01-01', 10, 'Construction', 90000.00, 8 );

INSERT INTO Item VALUES ('SmartElectric',1,'Indoor Non-metallic Wire',54.00);
INSERT INTO Item VALUES ('Wood Chippers',2,'Oakwood',95.00);
INSERT INTO Item VALUES ('Cheapo Concrete',3,'Ready-to-Use Concrete',50.00);
INSERT INTO Item VALUES ('Light Bright',4,'Standard Lightbulbs',15.00);
INSERT INTO Item VALUES ('Plumbing World',5,'Copper Drainage Pipes',970.00);
INSERT INTO Item VALUES ('Sturdy Metals',6,'Stainless Steel Countertops',300.00);
INSERT INTO Item VALUES ('Tractor Rent Depot',7,'Kubota Tractor',9500.00);
INSERT INTO Item VALUES ('Ventilation Dudes',8,'FSD Vent System',1300.00);
INSERT INTO Item VALUES ('Single Shingle',9,'Stone Roof Tiles',800.00);
INSERT INTO Item VALUES ('Tool Shed',10,'Wood Cutter Machine',500.00);
INSERT INTO Item VALUES ('Tool Shed',11,'Interior Paint',35.00);
INSERT INTO Item VALUES ('Tool Shed',12,'Casement Window',390.00);
INSERT INTO Item VALUES ('Tool Shed',13,'All Purpose Drywall',30.00);

INSERT INTO Quotation VALUES ('2017-01-02', '11:30:00', 'Wood Chippers', 2, 1, 'Construction', 'Flooring', 60, 5700.00);
INSERT INTO Quotation VALUES ('2017-01-06', '07:21:26', 'Tool Shed', 12, 1, 'Construction', 'Windows', 20, 7800.00);
INSERT INTO Quotation VALUES ('2017-01-07', '13:10:30', 'SmartElectric', 1, 5, 'Construction', 'Wiring', 20, 1080.00);
INSERT INTO Quotation VALUES ('2017-01-07', '18:14:12', 'Plumbing World', 5 , 3, 'Construction', 'Plumbing', 2 , 1940.00 );
INSERT INTO Quotation VALUES ('2017-01-08', '12:45:43', 'Light Bright', 4 , 6, 'Construction', 'Lighting', 35 , 525.00 );
INSERT INTO Quotation VALUES ('2017-01-09', '16:27:24', 'Single Shingle', 9 , 10, 'Construction', 'Roofing', 3 , 2400.00 );
INSERT INTO Quotation VALUES ('2017-01-10', '09:30:24', 'Tool Shed', 11 , 10, 'Construction', 'Painting', 20 , 700.00 );
INSERT INTO Quotation VALUES ('2017-01-10', '10:32:20', 'Tractor Rent Depot', 7 , 8, 'Construction', 'Excavation', 1 , 9500.00 );
INSERT INTO Quotation VALUES ('2017-01-10', '12:40:15', 'Ventilation Dudes', 8 , 4, 'Construction', 'HVAC', 3 , 3900.00 );
INSERT INTO Quotation VALUES ('2017-01-10', '13:39:00', 'Cheapo Concrete', 3 , 9, 'Construction', 'Carpentry', 30 , 1500.00 );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INSERT INTO Payment VALUES ( 1, 1500.00, '2017-01-02', '11:30:00', '2017-01-15 12:15:12', 'Wood Chippers', 2, 1, 'Construction', 'Flooring' );
INSERT INTO Payment VALUES ( 1, 1000.00, '2017-01-02', '11:30:00', '2017-01-30 12:15:12', 'Wood Chippers', 2, 1, 'Construction', 'Flooring' );
INSERT INTO Payment VALUES ( 2, 3900.00, '2017-01-06', '07:21:26', '2017-01-06 07:22:17', 'Tool Shed', 12, 1, 'Construction', 'Windows' );
INSERT INTO Payment VALUES ( 2, 3900.00, '2017-01-06', '07:21:26', '2017-01-06 12:00:00', 'Tool Shed', 12, 1, 'Construction', 'Windows' );
INSERT INTO Payment VALUES ( 3, 1080.00, '2017-01-07', '13:10:30', '2017-01-07 13:11:00',  'SmartElectric', 1, 5, 'Construction', 'Wiring' );
INSERT INTO Payment VALUES ( 4, 1000.00, '2017-01-07', '18:14:12', '2017-01-07 19:00:00',  'Plumbing World', 5 , 3, 'Construction', 'Plumbing' );
INSERT INTO Payment VALUES ( 5, 500.00, '2017-01-08', '12:45:43', '2017-01-08 13:01:07',  'Light Bright', 4 , 6, 'Construction', 'Lighting' );
INSERT INTO Payment VALUES ( 6, 1200.00, '2017-01-09', '16:27:24', '2017-01-09 16:28:20',  'Single Shingle', 9 , 10, 'Construction', 'Roofing');
INSERT INTO Payment VALUES ( 7, 700.00, '2017-01-10', '09:30:24', '2017-01-10 9:31:40',  'Tool Shed', 11 , 10, 'Construction', 'Painting');
INSERT INTO Payment VALUES ( 8, 4700.00, '2017-01-10', '10:32:20', '2017-01-10 10:32:21', 'Tractor Rent Depot', 7 , 8, 'Construction', 'Excavation');

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
TRIGGER

DELIMITER $$
CREATE TRIGGER `CalcAmount` BEFORE INSERT ON `payment` FOR EACH ROW
BEGIN
	UPDATE Quotation
	SET amountRemaining = (amountRemaining - NEW.amountPay)
	WHERE
	itemID = NEW.itemID AND
	supplierName = NEW.supplierName AND
	projectID = NEW.projectID AND
	phaseName = NEW.phaseName AND
	taskName = NEW.taskName AND
	dateOfQuote = NEW.dateOfQuote AND
	timeOfQuote = NEW.timeOfQuote;
END
$$
DELIMITER ;
